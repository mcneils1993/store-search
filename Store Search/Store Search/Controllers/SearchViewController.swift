//
//  ViewController.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-02-02.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

//Using constants for table cell identifiers
struct TableView {
    struct CellIdentifiers {
        static let searchResultCell = "SearchResultCell"
        static let nothingFoundCell = "NothingFoundCell"
        static let loadingCell = "LoadingCell"
    }
}

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
        performSearch()
    }
    
    var landscapeVC: LandscapeViewController?
    private let search = Search()
    
    weak var splitViewDetail: DetailViewController? 

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Decides if keyboard should display automatically for an iPad or iPhone. If not an iPad it will show upon loading this view
        if UIDevice.current.userInterfaceIdiom != .pad {
            searchBar.becomeFirstResponder()
        }
        
        // It lets the first row in tableView be visible, since the searchBar partially hides it
        tableView.contentInset = UIEdgeInsets(top: 99, left: 0, bottom: 0, right: 0)
        
        var cellNib = UINib(nibName: TableView.CellIdentifiers.searchResultCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: TableView.CellIdentifiers.searchResultCell)
        
        cellNib = UINib(nibName: TableView.CellIdentifiers.nothingFoundCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: TableView.CellIdentifiers.nothingFoundCell)
        
        cellNib = UINib(nibName: TableView.CellIdentifiers.loadingCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: TableView.CellIdentifiers.loadingCell)
        
        let segmentColor = UIColor(displayP3Red: 10/255, green: 80/255, blue: 80/255, alpha: 1)
        let normalTextAttributes = [NSAttributedString.Key.foregroundColor: segmentColor]
        let selectedTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        segmentedControl.selectedSegmentTintColor = segmentColor
        segmentedControl.setTitleTextAttributes(normalTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(selectedTextAttributes, for: .selected)
        segmentedControl.setTitleTextAttributes(selectedTextAttributes, for: .highlighted)
        
        //Back button title for NavigationController for are iPad when in portrait mode
        title = NSLocalizedString("Search", comment: "Split View Master Button")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" {
            /* Here we only care about the .result case, so writing an entire switch statement is not necessary.
               Instead we use the special if case statement to look at a single case. */
            if case .results(let list) = search.state {
                let detailViewController = segue.destination as! DetailViewController
                let indexPath = sender as! IndexPath
                let searchResult = list[indexPath.row]
                detailViewController.isPopUp = true
                detailViewController.searchResult = searchResult
            }
        }
    }
    
    /* This method isn't just invoked on device rotations, but anytime a trait collection for the view controller changes.
        A trait collection can be, hortizontal or vertical size class, display scale, a UI Idiom, or even dark to light mode.
    */
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)

        let rect = UIScreen.main.bounds
        //first conditional check is for portraint, second is for landscape
        if (rect.width == 736 && rect.height == 414) || (rect.width == 414 && rect.height == 736) {
            if presentedViewController != nil {
                dismiss(animated: true, completion: nil)
            }
        } else if UIDevice.current.userInterfaceIdiom != .pad {
            switch newCollection.verticalSizeClass {
            case .compact:
                showLandscape(with: coordinator)
            case .regular, .unspecified:
                /* On an iPad, both the horizatal and vertical size classes are always regular, regarless of device rotation.
                   This is why nothing happens when rotating the device. On iPhone pluses device, the horizontal
                   size classes are .regular when they are in landscape. NOTE THIS DOES NOT WORK ON IPHONE X, X'S ARE COMPACT */
                hideLandscape(with: coordinator)
            @unknown default:
                fatalError()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        performSearch()
    }
    
    //MARK:- Helper Methods
    func showNetworkError() {
        let alert = UIAlertController(title: NSLocalizedString("Whoops", comment: "Error alert: title"),
                                      message: NSLocalizedString("There was an error accessing the iTunes Store  Please try again", comment: "Error alert: message"),
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func showLandscape(with coordinator: UIViewControllerTransitionCoordinator) {
        //1
        guard landscapeVC == nil else { return }
        
        //2
        landscapeVC = storyboard?.instantiateViewController(identifier: "LandscapeViewController") as? LandscapeViewController
        if let controller = landscapeVC {
            
            /* Passing the search results to LandScapeViewController, has to be assigned before the view property so that its assigned before viewDidLoad gets called
               in the LandscapeViewController as viewDidLoad isn't called before. If we access controller.view before settings searchResults,
               this property will still be empty and no button will be created
             */
            controller.search = search
            //3
            controller.view.frame = view.bounds
            controller.view.alpha = 0
            //4
            view.addSubview(controller.view)
            addChild(controller)
            
            //does the crossfade animation
            coordinator.animate(alongsideTransition: { _ in
                if self.presentedViewController != nil { //when presenting are detail view controller if we aren't in landscape mode yet
                    self.dismiss(animated: true, completion: nil)
                }
                controller.view.alpha = 1
                self.searchBar.resignFirstResponder()
            }) { _ in
                controller.didMove(toParent: self)
            }
        }
    }
    
    func hideLandscape(with coordinator: UIViewControllerTransitionCoordinator) {
        if let controller = landscapeVC {
            controller.willMove(toParent: nil) //leaving the view controller hierarcy so that it no longer has a parent
            coordinator.animate(alongsideTransition: { _ in
                controller.view.alpha = 0
            }, completion: { _ in
                controller.view.removeFromSuperview()//remove it from the scene
                controller.removeFromParent() //disposes of the view controller
                self.landscapeVC = nil //remove the last strong reference
            })
        }
    }
    
    func hideMasterPane() {
        UIView.animate(withDuration: 0.25, animations: {
            self.splitViewController!.preferredDisplayMode = .primaryHidden
        }, completion: { _ in
            self.splitViewController!.preferredDisplayMode = .automatic
        })
    }
}

extension SearchViewController: UISearchBarDelegate {
    
    //Invoked when you tap the search button on the keyboard
    func performSearch() {
        if let category = Search.Category(rawValue: segmentedControl.selectedSegmentIndex) {
            
            //The code in this closure gets called after the search completes!!!
            search.performSearch(for: searchBar.text!, category: category, completion: { success in
                if !success {
                    self.showNetworkError()
                }
                self.tableView.reloadData()
                
                /* When the search begins there is no LandscapeViewController object yet because the only way to start a search is in portrait mode.
                   But by the time the closure is invoked, the device may have rotated and if that happened self.landscapceVc will contain a valid reference.
                 
                   Upon rotation, we also gave the new LandscapeViewController a reference to the active Search object. Now we just have to tell it that the search
                   results are available so it can create the buttons and fill them up with images.
                 
                   Of course, if we are still in portrait mode by the time the search completes, then self.landscapeVC is nil and the call to searchResultsRecieved will simply be ignored
                   due to the optional chaining
                 */
                self.landscapeVC?.searchResultsReceived()
            })
            tableView.reloadData()
            searchBar.resignFirstResponder()
        }
    }
    
    //Makes the status bar area unified with the search bar
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        /* Because .results has an array of SearchResult objects associated with it, you can bind this array to a temporary variable.
           This variable is list, and then we can use that variable inside the case to read how many items are in the array. This is how we make use of the associated value
         */
        switch search.state {
        case .notSearchedYet:
            return 0
        case .loading:
            return 1
        case .noResults:
            return 1
        case .results(let list):
            return list.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch search.state {
        case .notSearchedYet:
            fatalError("Should never get here")
            
        case .loading:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableView.CellIdentifiers.loadingCell, for: indexPath)
            let spinner = cell.viewWithTag(100) as! UIActivityIndicatorView
            spinner.startAnimating()
            return cell
            
        case .noResults:
             return tableView.dequeueReusableCell(withIdentifier: TableView.CellIdentifiers.nothingFoundCell, for: indexPath)
            
        case .results(let list):
            let cell = tableView.dequeueReusableCell(withIdentifier: TableView.CellIdentifiers.searchResultCell) as! SearchResultCell
            let searchResult = list[indexPath.row]
            cell.configure(for: searchResult)
            return cell
        }
    }
    
    //Simply deselect the row with an animation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        
        if view.window!.rootViewController!.traitCollection.horizontalSizeClass == .compact { //normal iPhone devices
            tableView.deselectRow(at: indexPath, animated: true)

            //putting the segue on the view controller itself to trigger the segue manually
            performSegue(withIdentifier: "ShowDetail", sender: indexPath)
        } else {
            if case .results(let list) = search.state {
                /* On the iPad, it assigns the SearchResult object to the existing DetailViewController that lives in the detail pane. */
                splitViewDetail?.searchResult = list[indexPath.row]
            }
            if splitViewController!.displayMode != .allVisible {
                hideMasterPane()
            }
        }
    }
    
    //Makes sure that you can only select rows when you have actual search results. Meaning if we tap on (Nothing Found) row, the cell won't turn gray at all.
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        switch search.state {
        case .notSearchedYet, .loading, .noResults:
            return nil
        case .results:
            return indexPath
        }
    }
}
