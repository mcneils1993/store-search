//
//  LandscapeViewController.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-03-20.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

class LandscapeViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var search = Search()
    private var firstTime = true
    
    private var downloads = [URLSessionDownloadTask]() //This array keeps track of all the active URLSessionDownloadTask objects
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //remove constraints from the main view
        view.removeConstraints(view.constraints)
        //If you want to use Auto Layout to dynamically calculate the size and position of your view, you must set this property to false
        view.translatesAutoresizingMaskIntoConstraints = true
        
        //remove constraints for page control
        pageControl.removeConstraints(pageControl.constraints)
        pageControl.translatesAutoresizingMaskIntoConstraints = true
        
        //remove constraints for scroll view
        scrollView.removeConstraints(scrollView.constraints)
        scrollView.translatesAutoresizingMaskIntoConstraints = true
        
        //Pattern image lets you create a background from a single squared image in your assets folder
        view.backgroundColor = UIColor(patternImage: UIImage(named: "LandscapeBackground")!)
        
        pageControl.numberOfPages = 0
      
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let safeFrame = view.safeAreaLayoutGuide.layoutFrame
        scrollView.frame = safeFrame
        
        
        pageControl.frame = CGRect(x: safeFrame.origin.x, y: safeFrame.size.height - pageControl.frame.size.height, width: safeFrame.size.width, height: pageControl.frame.size.height)
        
        //This is for when passing data from the SearchViewController to this view controller. See note in SearchViewController class in showLandscape
        if firstTime {
            firstTime = false
            switch search.state {
            case .notSearchedYet:
                break
            case .loading:
                showSpinner()
            case .noResults:
                showNothingLabel()
            case .results(let list):
                titleButtons(list)
            }
        }
    }
    
    //MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" {
            if case .results(let list) = search.state {
                let detailViewController = segue.destination as! DetailViewController
                let searchResult = list[(sender as! UIButton).tag - 2000]
                detailViewController.searchResult = searchResult
            }
        }
    }
    
    //MARK:- Private Methods
    private func titleButtons(_ searchResults: [SearchResult]) {
        var columnsPerPage = 6
        var rowsPerPage = 3
        var itemWidth: CGFloat = 94
        var itemHeight: CGFloat = 88
        var marginX: CGFloat = 2
        var marginY: CGFloat = 20
        
        let buttonWidth: CGFloat = 82
        let buttonHeight: CGFloat = 82
        let paddingHorz = (itemWidth - buttonWidth) / 2
        let paddingVert = (itemWidth - buttonHeight) / 2
        
        let viewWidth = scrollView.bounds.size.width
        
            switch viewWidth {
            case 568:
            // 4-inch device
            break
            
            case 667:
                // 4.7 inch device
                columnsPerPage = 7
                itemWidth = 95
                itemHeight = 98
                marginX = 1
                marginY = 29
                
            case 736:
                //5.5 inch device
                columnsPerPage = 8
                rowsPerPage = 4
                itemWidth = 92
                marginX = 0
                
            case 724:
                //iPhone X, iPhone 11 Pro
                columnsPerPage = 8
                rowsPerPage = 3
                itemWidth = 90
                itemHeight = 98
                marginX = 2
                marginY = 29
                
            default:
                break
        }
        
        //Adding the buttons
        var row = 0
        var column = 0
        var x = marginX
        for(index, result) in searchResults.enumerated() {
            //1
            let button = UIButton(type: .custom)
            button.setBackgroundImage(UIImage(named: "LandscapeButton"), for: .normal)
            
            //2
            button.frame = CGRect(x: x + paddingHorz, y: marginY + CGFloat(row)*itemHeight + paddingVert, width: buttonWidth, height: buttonHeight)
            
            if let downloadTask = button.downloadImage(for: result, andPlaceOn: button) {
                 downloads.append(downloadTask)
            }
            
            //3
            scrollView.addSubview(button)
            
            //4
            row += 1
            if row == rowsPerPage {
                row = 0
                x += itemWidth
                column += 1
                
                if column == columnsPerPage {
                    column = 0
                    x += marginX * 3
                }
            }
            button.tag = 2000 + index
            button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        }
        
        //Set scroll view content size, the number of white dots to be displayed at the bottom
        let buttonsPerPAge = columnsPerPage * rowsPerPage
        let numPages = 1 + (searchResults.count - 1) / buttonsPerPAge
        scrollView.contentSize = CGSize(width: CGFloat(numPages) * viewWidth, height: scrollView.bounds.size.height)
        print("\(scrollView.contentSize)")
        print("Number of pages : \(numPages)")
        
        pageControl.numberOfPages = numPages
        pageControl.currentPage = 0
    }
    
    private func showSpinner() {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.color = .white
        spinner.center = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        spinner.tag = 1000
        view.addSubview(spinner)
        spinner.startAnimating()
    }
    
    private func hideSpinner() {
        view.viewWithTag(1000)?.removeFromSuperview()
    }
    
    private func showNothingLabel() {
        let label = UILabel(frame: CGRect.zero)
        label.text = NSLocalizedString("Nothing Found", comment: "Nothing Found Landscape")
        label.textColor = UIColor.white
        
        //tells the label to resize itself to the optimal size. This helps when you're translating the app to a different language
        label.sizeToFit()
        
        label.center = CGPoint(x: view.bounds.midX, y: view .bounds.midY)
        view.addSubview(label)
    }
    
    //MARK:- Public Methods
    func searchResultsReceived() {
        hideSpinner()
        
        switch search.state {
        case .notSearchedYet, .loading:
            break
        case .noResults:
            showNothingLabel()
        case .results(let list):
            titleButtons(list)
        }
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowDetail", sender: sender)
    }

    //Stops the download for any button whos image was still pending or in transit
    deinit {
        print("deinit \(self)")
        for task in downloads {
            task.cancel()
        }
    }
    
    //MARK:- Actions
    @IBAction func pageChanged(_ sender: UIPageControl) {
        /* used to update the scroll view buttons, we linked this action to our scroll view in the Storyboard.
           When we tap on the bottom left or right side of the screen the page animates */
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut], animations: {
            self.scrollView.contentOffset = CGPoint(x: self.scrollView.bounds.size.width * CGFloat(sender.currentPage), y: 0)
        }, completion: nil)
    }
}

extension LandscapeViewController: UIScrollViewDelegate {
    
    /* In this function we figure out what the index of the current page is by looking at the contentOffset property of the scrollView
       This property determines how far the scroll view has been scrolled and is updated while you're dragging the scroll view.
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let width = scrollView.bounds.size.width
        let page = Int((scrollView.contentOffset.x + width / 2) / width) //used to calculate the current page number
        pageControl.currentPage = page
    }
}
