//
//  DetailViewController.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-02-16.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit
import MessageUI

class DetailViewController: UIViewController {
    
    enum AnimationStyle {
        case slide
        case fade
    }

    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var kindLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var priceButton: UIButton!

    var searchResult: SearchResult! {
        didSet {
            /* This isViewLoaded check ensures this property observer only gets used when on an iPad. This is necessary for iPad because
               since we can select something for the search list quickly as opposed to iPhone we need to close the detail pop up and then select again,
               the lables won't update properly on an iPad since we are selecting cell without the need of dimissing the detail view controller first
             */
            if isViewLoaded {
                updateUI()
                popupView.isHidden = false
            }
        }
    }
    var dismissStyle = AnimationStyle.fade //determines which animation is chosen
    
    /* Since we're using DetalViewController for both purposes, pop-up and detail pane, we need to give a Boolean that determines how it should behave.
       On the iPhone it will be pop-up, on the iPad it will not.
     */
    var isPopUp = false
    
    //create a URLSessionDownloadTask property to cancel the image download request if need to
    var downloadTask: URLSessionDownloadTask?


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /* tint color to apply to the button title and background image. Make sure in your assets the image that you select to put as a background for a button the "Render As" in the attributes inspector is set to Template Image */
        if (traitCollection.userInterfaceStyle == .light) {
            view.tintColor = UIColor(red: 20/255, green: 160/255, blue: 160/255, alpha: 1)
        } else {
            view.tintColor = UIColor(red: 140/255, green: 140/255, blue: 240/255, alpha: 1)
        }
        popupView.layer.cornerRadius = 10
        
        if isPopUp { //portrait for iPhone
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
            gestureRecognizer.cancelsTouchesInView = false
            gestureRecognizer.delegate = self
            view.addGestureRecognizer(gestureRecognizer)
            
            //makes are background transparent so that our gradient view looks the way its supposed to.
            view.backgroundColor = UIColor.clear
            
        } else { //landscape // portrait for iPad
            view.backgroundColor = UIColor(patternImage: UIImage(named: "LandscapeBackground")!)
            popupView.isHidden = true
            
            //We get the application name to display it as the app title in the white navigation bar when in landscape mode
            if let displayName = Bundle.main.localizedInfoDictionary?["CFBundleDisplayName"] as? String {
                title = displayName
            }
        }

        //The != is just a defensive measure
        if searchResult != nil {
            updateUI()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowMenu" {
            let controller = segue.destination as! MenuViewController
            controller.delegate = self
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }
    
    @IBAction func close() {
        dismissStyle = .slide
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openStore() {
        if let url = URL(string: searchResult.storeURL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    //MARK:- Helper Methods
    func updateUI() {
        
        if let largeURL = URL(string: searchResult.imageLarge) {
            downloadTask = artworkImageView.loadImage(url: largeURL)
        }
        
        nameLabel.text = searchResult.name
        
        if searchResult.artist.isEmpty {
            artistNameLabel.text = NSLocalizedString("Unknown", comment: "Unknown Artist Name")
        } else {
            artistNameLabel.text = searchResult.artist
        }
        kindLabel.text = searchResult.type
        genreLabel.text = searchResult.genre
        
        //Show Price
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = searchResult.currency
        
        let priceText: String
        if searchResult.price == 0 {
            priceText = "Free"
        } else if let text = formatter.string(from: searchResult.price as NSNumber) {
            priceText = text
        } else {
            priceText = ""
        }
        priceButton.setTitle(priceText, for: .normal)
    }
    
    deinit {
        downloadTask?.cancel()
    }
}

extension DetailViewController: UIViewControllerTransitioningDelegate {
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return DimmingPresentationController(presentedViewController: presented, presenting: presenting)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BounceAnimationController()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        //Switch statement that decides which animation controller should be used to dismiss the detail popUP
        switch dismissStyle {
        case .slide:
            return SlideOutAnimationController()
        case .fade:
            return FadeOutAnimationController()
        }
    }
}


//handles the dismissal of the detail pop up when we tap outside the detail pop display, before calling the close function above
extension DetailViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return (touch.view === self.view)
    }
}

extension DetailViewController: MenuViewControllerDelegate {
    func menuViewControllerSendEmail(_ controller: MenuViewController) {
        //the dismiss hide the popOver
        dismiss(animated: true) {
            if MFMailComposeViewController.canSendMail() {
                let controller = MFMailComposeViewController()
                controller.modalPresentationStyle = .formSheet
                controller.mailComposeDelegate = self
                controller.setSubject(NSLocalizedString("Support Request", comment: "Email subject"))
                controller.setToRecipients(["simonmcneil062@gmail.com"])
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
}

extension DetailViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}
