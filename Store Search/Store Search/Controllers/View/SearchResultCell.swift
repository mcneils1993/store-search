//
//  SearchResultCell.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-02-05.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var artworkImageView: UIImageView!
    
    var downloadTask: URLSessionDownloadTask?
    
    /* awakeFromNib method is called after the cell object has been loaded from the nib but before the cell is added to the tableView
        - The reason why we don't do this in init coder is because the UILabel will still be nil but in awake from nib they will be properly hooked up */
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let selectedview = UIView(frame: CGRect.zero)
        selectedview.backgroundColor = UIColor(displayP3Red: 20/255, green: 160/255, blue: 160/255, alpha: 1.0)
        selectedBackgroundView = selectedview
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        downloadTask?.cancel()
        downloadTask = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(for result: SearchResult) {
        nameLabel.text = result.name
        
        if result.artist.isEmpty {
            artistNameLabel.text = NSLocalizedString("Unknown", comment: "Unknown Artist Name")
        } else {
            artistNameLabel.text = String(format: NSLocalizedString("%@ (%@)", comment: "Format for artist name"), result.artist, result.type)
        }
        artworkImageView.image = UIImage(named: "Placeholder")
        if let smallURL = URL(string: result.imageSmall) {
            downloadTask = artworkImageView.loadImage(url: smallURL)
        }
    }

}
