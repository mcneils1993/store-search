//
//  SlideOutAnimationController.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-02-21.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

///This class takes care of the pop up exiting the view controller upwards
class SlideOutAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) {
            let containerView = transitionContext.containerView
            let time = transitionDuration(using: transitionContext)
            
            UIView.animate(withDuration: time, animations: {
                fromView.center.y -= containerView.bounds.size.height * 2
                fromView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: { finised in
                transitionContext.completeTransition(finised)
            })
        }
    }
}
