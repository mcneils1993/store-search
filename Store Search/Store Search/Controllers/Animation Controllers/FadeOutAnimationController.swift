//
//  FadeOutAnimationController.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-03-22.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

/* An animation controller that will allow a view to fade out */
class FadeOutAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
   
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) {
            let time = transitionDuration(using: transitionContext)
            UIView.animate(withDuration: time, animations: {
                fromView.alpha = 0
            }, completion: { finished in
                transitionContext.completeTransition(finished)
            })
        }
    }
}
