//
//  MenuViewController.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-04-29.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

protocol MenuViewControllerDelegate: class {
    func menuViewControllerSendEmail(_ controller: MenuViewController)
}

class MenuViewController: UITableViewController {

    //Like all delegate properties, this is weak because you don’t want MenuViewcontroller to “own” the object that implements the delegate methods.
    weak var delegate: MenuViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            print("Selected Send Support Email Row")
            delegate?.menuViewControllerSendEmail(self)
        }
    }
}
