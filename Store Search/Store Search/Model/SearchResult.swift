//
//  SearchResult.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-02-02.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation

class ResultArray: Codable {
    var resultCount = 0
    var results = [SearchResult]()
}

class SearchResult: Codable, CustomStringConvertible {
    var artistName: String? = ""
    var trackName: String? = ""
    var kind: String? = ""
    var trackPrice: Double? = 0.0
    var currency = ""
    var imageSmall = ""
    var imageLarge = ""
    var trackViewUrl: String?
    var collectionName: String?
    var collectionViewUrl: String?
    var collectionPrice: Double?
    var itemPrice: Double?
    var itemGenre: String?
    var bookGenre: [String]?
    
    private let typeForKind = [
        "album": NSLocalizedString("Album", comment: "Localized Kind: Album"),
        "audiobook": NSLocalizedString("Audio Book", comment: "Localized kind: Audio Book"),
        "book": NSLocalizedString("Book", comment: "Localized kind: Book"),
        "ebook": NSLocalizedString("E-Book", comment: "Localized kind: E-Book"),
        "feature-movie": NSLocalizedString("Movie", comment: "Localized Kind: Movie"),
        "music-video": NSLocalizedString("Music Video", comment: "Localized Kind: Music Video"),
        "podcast": NSLocalizedString("Podcast", comment: "Localized Kind: Podcast"),
        "software": NSLocalizedString("App", comment: "Localized Kind: App"),
        "song": NSLocalizedString("Song", comment: "Localized Kind: Stong"),
        "tv-episode": NSLocalizedString("TV Episode", comment: "Localized Kind: TV Episode")
    ]
    
    //Here we use CodingKeys enumeration to let the Codable protocol know how we want the SearchResult properties matched to the JSON
    enum CodingKeys: String, CodingKey {
        case imageSmall = "artworkUrl60"
        case imageLarge = "artworkUrl100"
        case itemGenre = "primaryGenreName"
        case bookGenre = "genres"
        case itemPrice = "price"
        case kind, artistName, currency
        case trackName, trackPrice, trackViewUrl
        case collectionName, collectionViewUrl, collectionPrice
    }
    
    var name: String {
        /* Chaining of the nil-coalescing operator. Check to see if trackName is nil, if not return unwrapped value of trackName.
         If it's nil then we move on to collectionName and do the same check. If both values are nil we return an empty string */
        return trackName ?? collectionName ?? ""
    }
    
    var storeURL: String {
        return trackViewUrl ?? collectionViewUrl ?? ""
    }
    
    var price: Double {
        return trackPrice ?? collectionPrice ?? itemPrice ?? 0.0
    }
    
    var genre: String {
        if let genre = itemGenre {
            return genre
        } else if let genres = bookGenre {
            return genres.joined(separator: ", ")
        }
        return ""
    }
    
    var description: String {
        return "Kind: \(kind ?? "None"), Name: \(name), Artist Name: \(artistName ?? "None")\n"
    }
    
    var type: String {
        let kind = self.kind ?? "audiobook"
        return typeForKind[kind] ?? kind
    }
    
    var artist: String  {
        return artistName ?? ""
    }
    
    //Operator Overloading for creating an ascending order functionality
    static func < (lhs: SearchResult, rhs: SearchResult) -> Bool {
        return lhs.name.localizedStandardCompare(rhs.name) == .orderedAscending
    }
    
}
