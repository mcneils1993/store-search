//
//  Search.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-04-08.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

/* This typealias lets us create a mode convenient name for a data type. Here we decalre a type for our own clousre.
   This is a closure that returns no value and takes on Bool parameter. So SearchComplete can be refered to as a closure
   that takes a Bool parameter and returns no value
 */
typealias SearchComplete = (Bool) -> Void

class Search {
    
    enum Category: Int {
        case all = 0
        case music = 1
        case software = 2
        case ebooks = 3
        
        var type: String {
            switch self {
            case .all: return ""
            case .music: return "musicTrack"
            case .software: return "software"
            case .ebooks: return "ebook"
            }
        }
    }
    
    enum State {
        case notSearchedYet
        case loading
        case noResults
        
        /* Array is only important when the search is successful.
           By making it an associated value, we only have access to this array when Search is in the .result state.
           In other states, the array does not exist */
        case results([SearchResult])
    }
    
    private var dataTask: URLSessionDataTask? = nil
    
    /* This keeps track of Search current state. This variable is private, but only half so. It's not unreasonale for other objects to want to ask
       Search waht its curent state is. In fact, the app won't work unless you allow this.
     
       But you don't want thos other objects to be able to cha ge the value of state; they are allowed to read the state value. With private(set) you tell Swift
       that reading is Ok for other objects, but settings new value to this variable may only happen in the Search class.
     */
    private(set) var state: State = .notSearchedYet
    
    /* The @escaping annotation is necessary for closures that are not used immediately. It tells swift that this closure may need to capture
       varaibles such as self and keep the around for a little while until the closure can be finally be executed, in the case, when the search is done.
     
       If we remove the @escaping keyword then we get an error message for the session.dataTask closure block and the DispatchQueue.main.async closure block
     */
    func performSearch(for text: String, category: Category, completion: @escaping SearchComplete) {
        if !text.isEmpty {

            //If no search has been done (first time user enters data) yet and dataTask is still nil, this simply ignores the cancel.
            dataTask?.cancel()
            
            state = .loading

            let url = itunesURL(searchText: text, category: category)

            /* URLSession is a closure-based API, meaning that instead of making a delegate, you pass it a closure
               containing the code that should be performing once the response from the server has been received. */
            let session = URLSession.shared
            dataTask = session.dataTask(with: url, completionHandler: { data, response, error in
                var newState = State.notSearchedYet
                var success = false
                if let error = error as NSError?, error.code == -999 {
                    return //Search was cancelled
                }
                
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let data = data {
                    var searchResults = self.parse(data: data)
                    if searchResults.isEmpty {
                        newState = .noResults
                    } else {
                        searchResults.sort(by: <)
                        //The array object is intrinsically attached to the value of newState, and the copy this valye to self.state in the main thread below
                        newState = .results(searchResults)
                    }
                    success = true
                    DispatchQueue.main.async {
                        /* The reason why we created a new variable and assigned it to the field property we declard at the top, is to change
                         state in the main thread to prevent a race condition. When we have multiple threads trying to use the same variable at the
                         same time, the app may do unexpected things. */
                        self.state = newState
                        completion(success)
                    }
                } else {
                    print("Failiure! \(response!)")
                }
            })
            dataTask?.resume()
        }
    }
    
    //MARK:- Helper Methods
    private func itunesURL(searchText: String, category: Category) -> URL {
        //Next three lines of code get the Localization info for the app
        let locale = Locale.autoupdatingCurrent
        let language = locale.identifier
        let countryCode = locale.regionCode ?? "US"
        
        let kind = category.type
        
        let encodedText = searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlString = "https://itunes.apple.com/search?" + "term=\(encodedText)&limit=100&entity=\(kind)" + "&lang=\(language)&country=\(countryCode)"
        
        let url = URL(string: urlString)
        return url! //url is a failable init so we must force unwrap it
    }
    
    private func parse(data: Data) -> [SearchResult] {
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(ResultArray.self, from: data)
            return result.results
        } catch {
            print("JSON Error: \(error)")
            return []
        }
    }
}
