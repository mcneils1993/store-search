//
//  UIImageView+DownloadImage.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-02-09.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func loadImage(url: URL) -> URLSessionDownloadTask {
        let session = URLSession.shared
        
        //1
        let downloadTask = session.downloadTask(with: url, completionHandler: { [weak self] url, response, error in
            //2 & 3
            if error == nil, let url = url, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                //4
                DispatchQueue.main.async {
                    if let weakSelf = self {
                        weakSelf.image = image
                    }
                }
            }
        })
        //5
        downloadTask.resume()
        return downloadTask
    }
}


extension UIButton {
    //used to download images and set the image as a background image for a button
    func downloadImage(for searchResult: SearchResult, andPlaceOn button: UIButton) -> URLSessionDownloadTask? {
        if let url = URL(string: searchResult.imageSmall) {
            let task = URLSession.shared.downloadTask(with: url) { [weak button] url, response, error in
                if error == nil, let url = url, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        if let button = button {
                            button.setImage(image, for: .normal)
                        }
                    }
                }
            }
            task.resume()
            return task
        }
        return nil
    }
}
