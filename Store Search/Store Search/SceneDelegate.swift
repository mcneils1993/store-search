//
//  SceneDelegate.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-04-14.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let _ = (scene as? UIWindowScene) else { return }
        detailVC.navigationItem.leftBarButtonItem = splitVC.displayModeButtonItem
        searchVC.splitViewDetail = detailVC
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    //The top-level view controller
    var splitVC: UISplitViewController {
        return window!.rootViewController as! UISplitViewController
    }
    
    //The Search screen in the master pane of the split
    var searchVC: SearchViewController {
        return splitVC.viewControllers.first as! SearchViewController
    }
    
    //The UINavigationController in the detail pane of the split view
    var detailNavViewController: UINavigationController {
        return splitVC.viewControllers.last as! UINavigationController
    }
    
    //The Detail screen inside the UINavigation controller
    var detailVC: DetailViewController {
        return detailNavViewController.topViewController as! DetailViewController
    }
}
