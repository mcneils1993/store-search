//
//  GradientView.swift
//  Store Search
//
//  Created by Simon McNeil on 2020-02-17.
//  Copyright © 2020 Simon McNeil. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
    //Used to create the gradient view instance. Essentially are constructor.
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
        
        /* Tells the view that it should change both its width and its height proportionally when the superview it belongs to resizes due to it being rotated or other reasons */
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    //Not actually using this, UIView demands that all subclasses implement this. That's why there is the required.
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    override func draw(_ rect: CGRect) {
        //1
        let components: [CGFloat] = [0, 0, 0, 0.3, 0, 0, 0, 0.7]
        let locations: [CGFloat] = [0, 1]
        
        //2
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorSpace: colorSpace, colorComponents: components, locations: locations, count: 2)
        
        //3
        let xMin = bounds.midX
        let yMin = bounds.midY
        let centerPoint = CGPoint(x: xMin, y: yMin)
        
        let radius = max(xMin, yMin)
        
        //4
        let context = UIGraphicsGetCurrentContext()
        context?.drawRadialGradient(gradient!, startCenter: centerPoint, startRadius: 0, endCenter: centerPoint, endRadius: radius, options: .drawsAfterEndLocation)
    }
}
